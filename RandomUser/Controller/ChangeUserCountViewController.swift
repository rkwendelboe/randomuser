//
//  ChangeUserCountViewController.swift
//  RandomUser
//
//  Created by Roman Wendelboe on 4/7/18.
//  Copyright © 2018 Roman Wendelboe. All rights reserved.
//

import UIKit

// MARK: Protocol

protocol  ChangedNumberOfUsers: class {
    func newNumberOfUsers(num: Int)
}

class ChangeUserCountViewController: UIViewController {
    
    // MARK: Properties
    
    weak var delegate: ChangedNumberOfUsers?
    
    // MARK: Outlets

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var currentNumberOfUsersRetrievedLabel: UILabel!
    
    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUp()
    }
    
    // MARK: Actions
    
    @IBAction func clickedNewNumber(_ sender: UIButton) {
        if textField.text == "" {
            delegate?.newNumberOfUsers(num: 10)
        } else {
            delegate?.newNumberOfUsers(num: Int(textField.text!)!)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: Functions
    
    private func setUp(){
        self.hideKeyboard()
        
        let num = UserDefaults.standard.getNumberOfResults()
        
        self.currentNumberOfUsersRetrievedLabel.text = num == 1 ?  "\(num) User" :  "\(num) Users"
    }
}
