//
//  ViewController.swift
//  RandomUser
//
//  Created by Roman Wendelboe on 4/7/18.
//  Copyright © 2018 Roman Wendelboe. All rights reserved.
//

import UIKit
import os.log

class ViewController: UIViewController, ActivityIndicatorPresenter {
    
    // MARK: Properties
    
    var users = [User]()
    var activityIndicatorView : UIView = UIView()
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.long
       return dateFormatter
    }()
    
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(ViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()
    
    // MARK: Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setUp()
        manageNumberOfResults()
        getUsers(needsNewUsers: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let index = self.tableView.indexPathForSelectedRow{
            self.tableView.deselectRow(at: index, animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let goNext = segue.destination as? ChangeUserCountViewController else {return}
        goNext.delegate = self
    }
    
    // MARK: Functions
    
    private func setUp(){
        // Add refreshControl
        tableView.addSubview(refreshControl)
        
        // Configure TableView
        tableView.register(UINib(nibName: "RandomUserTableViewCell", bundle: nil), forCellReuseIdentifier: "RandomUserTableViewCell")
        tableView.estimatedRowHeight = 85.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        showActivityIndicator()
    }
    
    @objc private func handleRefresh(_ refreshControl: UIRefreshControl) {
        getUsers(needsNewUsers: true)
    }
    
    fileprivate func getUsers(needsNewUsers: Bool){
        // Get Users
        DispatchQueue.global().async {
            if needsNewUsers {
                // Load new uesrs
                self.loadDataFromWeb()
            } else {
                // Check if there are saved users
                if let savedUsers = self.loadUsers() {
                    self.users += savedUsers
                    DispatchQueue.main.async {
                        self.refreshControl.endRefreshing()
                        self.tableView.reloadData()
                        self.hideActivityIndicator()
                    }
                    
                } else {
                    self.loadDataFromWeb()
                }
            }
        }
    }
    
    private func loadDataFromWeb(){
        // Construct Url
        guard let gitUrl = URL(string: Networking.api + "&results=\(UserDefaults.standard.getNumberOfResults())") else {
            print("Error: No data to decode")
            return
        }
        
        // Make Network call to get data
        URLSession.shared.dataTask(with: gitUrl) { (data, response, error) in
            guard let data = data else { return }
            
            do {
                let decoder = JSONDecoder()
                let usersData = try decoder.decode(ResultUsers.self, from: data)
                let newUsers = usersData.results.map{ User(from: $0)}
                
                self.hideActivityIndicator()

                DispatchQueue.main.async {
                    self.users = newUsers
                    let updateString = "Last Updated at " + self.dateFormatter.string(from: Date())
                    self.refreshControl.attributedTitle = NSAttributedString(string: updateString)
                    self.refreshControl.endRefreshing()
                    self.saveUsers()
                    self.tableView.reloadData()
                }
            } catch let err {
                print("Error: ", err)
                self.refreshControl.endRefreshing()
                self.hideActivityIndicator()
            }
            }.resume()
    }
    
    private func manageNumberOfResults(){
        if UserDefaults.standard.getNumberOfResults() == 0 {
            UserDefaults.standard.setnumberOfResults(value: 10)
        }
    }
    
    private func saveUsers() {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(users, toFile: User.ArchiveURL.path)
        if isSuccessfulSave {
            os_log("Users successfully saved.", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save users...", log: OSLog.default, type: .error)
        }
    }
    
    private func loadUsers() -> [User]?  {
        return NSKeyedUnarchiver.unarchiveObject(withFile: User.ArchiveURL.path) as? [User]
    }

}

// MARK: ChangedNumberOfUsers Protocol

extension ViewController: ChangedNumberOfUsers{
    func newNumberOfUsers(num: Int) {
        self.showActivityIndicator()
        UserDefaults.standard.setnumberOfResults(value: num)
        self.getUsers(needsNewUsers: true)
    }
}

// MARK: TableViewDataSource

extension ViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RandomUserTableViewCell", for: indexPath) as? RandomUserTableViewCell else {
            return UITableViewCell()
        }
        
        cell.user = self.users[indexPath.row]
        
        return cell
    }
}

// MARK: TableViewDelegate
extension ViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc: DetailedUserViewController = DetailedUserViewController.instanceFromStoryboard("Main")
        vc.user = self.users[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

