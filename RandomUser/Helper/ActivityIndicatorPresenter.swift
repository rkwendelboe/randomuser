//
//  ActivityIndicatorPresenter.swift
//  RandomUser
//
//  Created by Roman Wendelboe on 4/8/18.
//  Copyright © 2018 Roman Wendelboe. All rights reserved.
//

import UIKit

public protocol ActivityIndicatorPresenter {
    
    // The activity indicator
    var activityIndicatorView: UIView { get }
    //var activityIndicator: UIActivityIndicatorView { get }
    
    // Show the activity indicator in the view
    func showActivityIndicator()
    
    // Hide the activity indicator in the view
    func hideActivityIndicator()
}

public extension ActivityIndicatorPresenter where Self: UIViewController {
    
    func showActivityIndicator() {
        DispatchQueue.main.async {
            self.activityIndicatorView.backgroundColor = UIColor.darkGray
            self.activityIndicatorView.layer.cornerRadius = 8
            self.activityIndicatorView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
            self.activityIndicatorView.center = CGPoint(x: self.view.bounds.size.width / 2, y: self.view.bounds.height / 2)

            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            activityIndicator.color = UIColor.white
            activityIndicator.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
            activityIndicator.center = CGPoint(x: self.activityIndicatorView.bounds.size.width / 2, y: self.activityIndicatorView.bounds.height / 2)
            
            self.activityIndicatorView.addSubview(activityIndicator)
            self.view.addSubview(self.activityIndicatorView)
            activityIndicator.startAnimating()
        }
    }
    
    func hideActivityIndicator() {
        DispatchQueue.main.async {
            self.activityIndicatorView.removeFromSuperview()
        }
    }
}
