//
//  Extensions.swift
//  RandomUser
//
//  Created by Roman Wendelboe on 4/7/18.
//  Copyright © 2018 Roman Wendelboe. All rights reserved.
//

import UIKit

extension UIViewController {
    func hideKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    class func instanceFromStoryboard<T>(_ storyboardName:String) -> T {
        return UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: String(describing: T.self)) as! T
    }
}

extension UserDefaults{
    
    // MARK: Check Number of Results
    func setnumberOfResults(value: Int) {
        set(value, forKey: UserDefaultsKeys.numberOfResults.rawValue)
    }
    
    func getNumberOfResults()-> Int {
        return integer(forKey: UserDefaultsKeys.numberOfResults.rawValue)
    }
}

enum UserDefaultsKeys : String {
    case numberOfResults
}
