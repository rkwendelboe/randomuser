//
//  SlideToPopTransition.swift
//  RandomUser
//
//  Created by Roman Wendelboe on 4/8/18.
//  Copyright © 2018 Roman Wendelboe. All rights reserved.
//

import UIKit

class SlideToPopTransition: UINavigationController, UINavigationControllerDelegate {
    
    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        self.view.addGestureRecognizer(self.fullScreenPanGestureRecognizer)
    }
    
    // MARK: Functions
    
    private lazy var fullScreenPanGestureRecognizer: UIPanGestureRecognizer = {
        let gestureRecognizer = UIPanGestureRecognizer()
        
        if let cachedInteractionController = self.value(forKey: "_cachedInteractionController") as? NSObject {
            let string = "handleNavigationTransition:"
            let selector = Selector(string)
            if cachedInteractionController.responds(to: selector) {
                gestureRecognizer.addTarget(cachedInteractionController, action: selector)
            }
        }
        
        return gestureRecognizer
    }()
    
    // MARK: UINavigationControllerDelegate
    func navigationController(_: UINavigationController, didShow _: UIViewController, animated _: Bool) {
        self.fullScreenPanGestureRecognizer.isEnabled = self.viewControllers.count > 1
    }
}
