//
//  User.swift
//  RandomUser
//
//  Created by Roman Wendelboe on 4/7/18.
//  Copyright © 2018 Roman Wendelboe. All rights reserved.
//

import UIKit

struct ResultUsers: Codable {
    let results: [UserInfo]
    
    struct UserInfo: Codable {
        let name: Name
        let email: String
        let phone: String
        let location: Location
        let picture: Picture
        
        struct Name: Codable{
            let first: String
            let last: String
        }
        
        struct Location: Codable {
            let street: String
            let city: String
            let state: String
            let postcode: Int
        }
        
        struct Picture: Codable {
            let large: URL
            let thumbnail: URL
        }
    }
}

struct Networking {
    static let api = "https://randomuser.me/api/?nat=us&inc=name,email,phone,location,picture"
}
