//
//  User.swift
//  RandomUser
//
//  Created by Roman Wendelboe on 4/8/18.
//  Copyright © 2018 Roman Wendelboe. All rights reserved.
//

import UIKit
import os.log

class User: NSObject, NSCoding {
    
    //MARK: Properties
    
    var fullName: String?
    var email: String?
    var phone: String?
    var streetAddress: String?
    var cityStatePostal: String?
    var thumbnailUrl: URL?
    var largeUrl: URL?
    
    //MARK: Archiving Paths
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("users")
    
    //MARK: Types
    
    struct PropertyKey {
        static let fullName = "fullName"
        static let email = "email"
        static let phone = "phone"
        static let streetAddress = "streetAddress"
        static let cityStatePostal = "cityStatePostal"
        static let thumbnailUrl = "thumbnailUrl"
        static let largeUrl = "largeUrl"
    }
    
    //MARK: Initialization
    
    init?(fullName: String?, email: String?, phone: String?, streetAddress: String?, cityStatePostal: String?, thumbnailUrl: URL?, largeUrl: URL?) {
        // Initialize stored properties.
        self.fullName = fullName
        self.email = email
        self.phone = phone
        self.streetAddress = streetAddress
        self.cityStatePostal = cityStatePostal
        self.thumbnailUrl = thumbnailUrl
        self.largeUrl = largeUrl
    }
    
    init(from user: ResultUsers.UserInfo) {
        self.fullName = (user.name.first + " " + user.name.last).capitalized
        self.email = user.email
        self.phone = user.phone
        self.streetAddress = (user.location.street).capitalized
        self.cityStatePostal = (user.location.city + ", " + user.location.state + " " + String(user.location.postcode)).capitalized
        self.thumbnailUrl = user.picture.thumbnail
        self.largeUrl = user.picture.large
    }
    
    //MARK: NSCoding
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(fullName, forKey: PropertyKey.fullName)
        aCoder.encode(email, forKey: PropertyKey.email)
        aCoder.encode(phone, forKey: PropertyKey.phone)
        aCoder.encode(streetAddress, forKey: PropertyKey.streetAddress)
        aCoder.encode(cityStatePostal, forKey: PropertyKey.cityStatePostal)
        aCoder.encode(thumbnailUrl, forKey: PropertyKey.thumbnailUrl)
        aCoder.encode(largeUrl, forKey: PropertyKey.largeUrl)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let fullName = aDecoder.decodeObject(forKey: PropertyKey.fullName) as? String
        let email = aDecoder.decodeObject(forKey: PropertyKey.email) as? String
        let phone = aDecoder.decodeObject(forKey: PropertyKey.phone) as? String
        let streetAddress = aDecoder.decodeObject(forKey: PropertyKey.streetAddress) as? String
        let cityStatePostal = aDecoder.decodeObject(forKey: PropertyKey.cityStatePostal) as? String
        let thumbnailUrl = aDecoder.decodeObject(forKey: PropertyKey.thumbnailUrl) as? URL
        let largeUrl = aDecoder.decodeObject(forKey: PropertyKey.largeUrl) as? URL

        // Must call designated initializer.
        self.init(fullName: fullName, email: email, phone: phone, streetAddress: streetAddress, cityStatePostal: cityStatePostal, thumbnailUrl: thumbnailUrl, largeUrl: largeUrl)
    }
}
