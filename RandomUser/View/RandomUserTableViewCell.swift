//
//  RandomUserTableViewCell.swift
//  RandomUser
//
//  Created by Roman Wendelboe on 4/7/18.
//  Copyright © 2018 Roman Wendelboe. All rights reserved.
//

import UIKit

class RandomUserTableViewCell: UITableViewCell {
    
    // MARK: Properties
    
    var user: User? {
        didSet{
            guard let user = user else {
                print("Error: User not found!")
                return
            }
            
            if let rImage = user.thumbnailUrl {
                let data = try? Data(contentsOf: rImage)
                guard let image: UIImage = UIImage(data: data!) else { return }
                self.userImageView.image = image
            }
            
            if let fullName = user.fullName {
                self.fullNameLabel.text = fullName
            }
            
            if let email = user.email {
                self.emailLabel.text = email
            }
            
            if let phone = user.phone {
                self.phoneLabel.text = phone
            }
            
            if let streetAddress = user.streetAddress {
                self.streetAddressLabel.text = streetAddress
            }
            
            if let cityStatePostal = user.cityStatePostal {
                self.cityStatePostalLabel.text = cityStatePostal
            }
        }
    }
    
    // MARK: Outlets
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var streetAddressLabel: UILabel!
    @IBOutlet weak var cityStatePostalLabel: UILabel!
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: Functions
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        userImageView.image = UIImage(named: "avatar")
        fullNameLabel.text = ""
        emailLabel.text = ""
        phoneLabel.text = ""
        streetAddressLabel.text = ""
        cityStatePostalLabel.text = ""
    }
}
